import pkg from './package'

require('dotenv').config()

export default {
  mode: 'universal',

  /*
   ** Custom port
   */
  server: {
    port: 9999
  },

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    script: [
      { src: 'https://alvarotrigo.com/fullPage/vendors/scrolloverflow.js' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://unpkg.com/fullpage.js/dist/fullpage.min.css'
      }
    ],
    bodyAttrs: {
      class: 'hidden'
    }
  },

  /*
   ** Global page transitions
   */
  transition: {
    name: 'page',
    mode: 'out-in',
  },

  /*
   ** Customize the loading bar and initial loading
   */
  loading: '~/components/Loading/Loading.vue',

  // custom loading indicator once we have icon/logo set up.
  // loadingIndicator: {
  //   name: '@/view/loading.html'
  // },

  /*
   ** Global CSS
   */
  css: ['~/assets/css/style.css'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/vue-fullpage.js',
      ssr: false
    },
    '~/plugins/vue-moment.js'
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/vuetify',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-139848382-1'
      }
    ],
    '@nuxtjs/auth',
    '@nuxtjs/toast',
    '@nuxtjs/dotenv'
  ],

  /*
   ** Toast Notifications settings
   */
  toast: {
    position: 'bottom-right',
  },

  /*
   ** Vuetify options
   */
  vuetify: {
    //  theme: { }
  },

  /*
   ** Axios module configuration
   */
  axios: {
    baseURL: process.env.API_URL,
    debug: true
  },

  /*
   ** Auth module configuration
   */
  auth: {
    redirect: {
      login: '/auth/login',
      logout: '/',
      callback: '/auth/login',
      home: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: 'auth/local', method: 'post', propertyName: 'jwt' },
          user: { url: 'users/me', method: 'get', propertyName: false },
          logout: false
        },
        tokenRequired: true,
        tokenType: 'Bearer'
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: true,
    extend (config, { isDev, isClient }) {}
  }
}
