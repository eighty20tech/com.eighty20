export const state = () => ({
  messages: []
})

export const getters = {
  messages(state) {
    return state.messages
  }
}

export const actions = {

  //fetching all messages
  async fetchMessages ({ commit }) {
    const messages = await this.$axios.$get('messages')
    commit("setMessages", messages)
  },

  //posting a single message
  async postMessage (vuexContext, commit ) {
    var payload = vuexContext.rootState.MessagePayload
    this.$axios.$post('messages', payload)
  },

}

export const mutations = {
  setMessages(state, messages) {
    state.messages = messages
  }
}
